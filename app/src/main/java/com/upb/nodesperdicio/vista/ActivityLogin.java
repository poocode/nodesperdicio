package com.upb.nodesperdicio.vista;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.upb.nodesperdicio.R;
import com.upb.nodesperdicio.controlador.ControladorLogin;
import com.upb.nodesperdicio.databinding.ActivityMainBinding;
import com.upb.nodesperdicio.interfaz.LoginIterface;

import android.text.Html;
import android.view.View;
import android.widget.Toast;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener, LoginIterface.View {

    private ActivityMainBinding binding;
    private ControladorLogin controladorLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //Implementación subrayada del texto registrarse
        binding.include.txtRegistrarse.setText(Html.fromHtml(getResources().getString(R.string.registrarse)));

        //Listener del boton
        binding.include.btnIngresar.setOnClickListener(this);

        // Inicializamos el controlador
        controladorLogin = new ControladorLogin(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnIngresar) {
            if (controladorLogin.validarLogin(binding.include.editUsuario.getText().toString(), "usuario")) {
                if (controladorLogin.validarLogin(binding.include.editPassword.getText().toString(), "password")) {
                    controladorLogin.usuarioPermitido(binding.include.editUsuario.getText().toString(), binding.include.editPassword.getText().toString());
                }
            }
        }
    }

    @Override
    public void validarResultado(String editText, String mensaje) {
        if (editText.equals("usuario")) {
            binding.include.editUsuario.setFocusable(true);
            binding.include.editUsuario.setFocusableInTouchMode(true);
            binding.include.editUsuario.requestFocus();
            binding.include.editUsuario.setError(mensaje);
        } else if (editText.equals("password")) {
            binding.include.editPassword.setFocusable(true);
            binding.include.editPassword.setFocusableInTouchMode(true);
            binding.include.editPassword.requestFocus();
            binding.include.editPassword.setError(mensaje);
        }

    }

    @Override
    public void usuarioAutorizado(Boolean valida) {
        if (valida) {
            Toast.makeText(this, "Usuario autorizado", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Usuario/Contraseña incorectos", Toast.LENGTH_LONG).show();
        }
    }
}